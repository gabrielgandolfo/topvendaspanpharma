// Agency Theme JavaScript

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function(){
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    $(window).scroll(function(){
        if($(this).scrollTop()>=100){
            $('#logo-top').addClass('logo-menu');
        }else{
            $('#logo-top').removeClass('logo-menu');
        }
    });

    $(function () {
      $('.dropdown-menu').dropdown('toggle');
    });

    $('#fat-menu').on('hide.bs.dropdown', function () {
        return false;
    });


    $('.carousel-control').on('click', function(){
        if($('header').attr('id') == 'banner1'){
            $('header').attr('id','banner2');
            $('#box-header-fotos').show();
        }else{
            $('header').attr('id','banner1');
            $('#box-header-fotos').hide();
        }

    })

})(jQuery); // End of use strict
